/**
 * React Native Demo App
 * http://www.technik-tipps-und-tricks.de/programmierung/javascript/react-native
 *
 * @format
 */

import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Root } from 'native-base';
import configureStore from './src/configureStore';
import { DefaultProps } from './src/types/default';
import Router from './src/AppContainer';
import I18nGate from './src/i18nGate';

const { store, persistor } = configureStore();

const App = (): React.FunctionComponentElement<DefaultProps> => {

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <I18nGate>
          <Root>
            <Router />
          </Root>
        </I18nGate>
      </PersistGate>
    </Provider>
  );
};

export default App;
