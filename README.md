ReactNativeDemo is a demonstration app for the React Native use.
A description and tutorial are available on the web:
http://www.technik-tipps-und-tricks.de/programmierung/javascript/react-native/


Release Notes:
1.0.3
  - added About page and NativeBase Vector Icons prepared page.
1.0.2
  - added Menu and More Sidebar DrawerNavigations and toggle events to Header buttons
1.0.1
  - changed app icon and version string
  - added react-navigation with Home, Settings, Info and Dummy screens
1.0.0
  - initial empty React Native TypeScript project