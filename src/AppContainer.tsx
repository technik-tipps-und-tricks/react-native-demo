/**
 * The app container holds the app navigation tree and screens
 * 
 * @format
 */

import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import MenuSidebar from './components/MenuSidebar';
import MoreSidebar from './components/MoreSidebar';
import Home from './screens/Home';
import NBVectorIcons from './screens/NBVectorIcons';
import Settings from './screens/Settings';
import About from './screens/About';

const MoreNavigator = createDrawerNavigator(
  {
    Settings: { screen: Settings },
    About: { screen: About }
  }, 
  {
    contentComponent: (props) => <MoreSidebar {...props} />,
    drawerPosition: 'right',
    drawerType: 'slide',
  }
);

const MenuNavigator = createDrawerNavigator(
  {
    Home: { screen: Home },
    NBVectorIcons: { screen: NBVectorIcons },
  }, 
  {
    contentComponent: (props) => <MenuSidebar {...props} />,
    drawerPosition: 'left',
    drawerType: 'slide',
  }
);

const RootNavigator = createSwitchNavigator(
  {
    Menu: { screen: MenuNavigator },
    More: { screen: MoreNavigator },
  }, 
  {
  //contentComponent: MenuSidebar,
  //drawerPosition: 'left',
  //initialRouteName: 'Content'
  }
);

const Router = createAppContainer(RootNavigator);

export default Router;

