export const SET_SELECTED_LANGUAGE = 'APP/SET_SELECTED_LANGUAGES';
export const SET_PASSWORD = 'APP/SET_PASSWORD';

export type AppActionTypes = SetSelectedLanguage | SetPassword;

interface SetSelectedLanguage {
  type: typeof SET_SELECTED_LANGUAGE;
  payload: SetSelectedLanguagePayload;
}

interface SetSelectedLanguagePayload {
  selectedLanguage: string;
}

interface SetPassword {
  type: typeof SET_PASSWORD;
  payload: SetPasswordPayload;
}

interface SetPasswordPayload {
  password: string;
}

export function setSelectedLanguage(selectedLanguage: string): SetSelectedLanguage {
  return {
    type: SET_SELECTED_LANGUAGE,
    payload: {
      selectedLanguage,
    },
  };
}

export function SetPassword(password: string): SetPassword {
  return {
    type: SET_PASSWORD,
    payload: {
      password,
    },
  };
}
