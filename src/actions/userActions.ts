export const SET_FIRST_NAME = 'USER/SET_FIRST_NAME';
export const SET_LAST_NAME = 'USER/SET_LAST_NAME';
export const SET_PHONE_NUMBER = 'USER/SET_PHONE_NUMBER';
export const SET_EMAIL_ADDRESS = 'USER/SET_EMAIL_ADDRESS';

export type UserActionTypes = SetFirstName | SetLastName | SetPhoneNumber | SetEmailAddress;

interface SetFirstName {
  type: typeof SET_FIRST_NAME;
  payload: SetFirstNamePayload;
}

interface SetFirstNamePayload {
  firstName: string;
}

interface SetLastName {
  type: typeof SET_LAST_NAME;
  payload: SetLastNamePayload;
}

interface SetLastNamePayload {
  lastName: string;
}

interface SetPhoneNumber {
  type: typeof SET_PHONE_NUMBER;
  payload: SetPhoneNumberPayload;
}

interface SetPhoneNumberPayload {
  phoneNumber: string;
}

interface SetEmailAddress {
  type: typeof SET_EMAIL_ADDRESS;
  payload: SetEmailAddressPayload;
}

interface SetEmailAddressPayload {
  emailAddress: string;
}

export function setFirstName(firstName: string): SetFirstName {
  return {
    type: SET_FIRST_NAME,
    payload: {
      firstName,
    },
  };
}

export function setLastName(lastName: string): SetLastName {
  return {
    type: SET_LAST_NAME,
    payload: {
      lastName,
    },
  };
}

export function setPhoneNumber(phoneNumber: string): SetPhoneNumber {
  return {
    type: SET_PHONE_NUMBER,
    payload: {
      phoneNumber,
    },
  };
}

export function setEmailAddress(emailAddress: string): SetEmailAddress {
  return {
    type: SET_EMAIL_ADDRESS,
    payload: {
      emailAddress,
    },
  };
}
