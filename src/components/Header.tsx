import React from 'react';
import {
  Body, Button, Header as NativeHeader, Icon, Left, Right, Text, Title,
} from 'native-base';
import { StyleSheet, Alert } from 'react-native';
import { DefaultScreenProps } from '../types/default';
import { DrawerActions } from 'react-navigation-drawer';

interface HeaderProps extends DefaultScreenProps{
  title: string;
}

const Header = (props: HeaderProps): React.FunctionComponentElement<HeaderProps> => {
  //const { navigation } = props;
  //const { title } = props;

  const onClickMenuButton = (): void => {
    props.navigation.navigate({routeName: 'Menu', action: DrawerActions.toggleDrawer()});
  };

  const onClickMoreButton = (): void => {
    props.navigation.navigate({routeName: 'More', action: DrawerActions.toggleDrawer()});
  };

  return (
    <NativeHeader>
      <Left style={styles.iconWrapper}>
        <Button transparent onPress={onClickMenuButton}>
          <Icon name="menu" />
        </Button>
      </Left>
      <Body>
        <Title>
          <Text style={styles.headerText}>{props.title}</Text>
        </Title>
      </Body>
      <Right style={styles.iconWrapper}>
        <Button transparent onPress={onClickMoreButton}>
          <Icon name="more" />
        </Button>
      </Right>
    </NativeHeader>
  );
};

const styles = StyleSheet.create({
  iconWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    width: 200,
    flexDirection: 'row',
    flexWrap: 'nowrap'
  }
});

export default Header;
