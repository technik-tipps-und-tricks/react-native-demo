/**
 * The right options navigation sidebar
 * 
 * @format
 */

import React from 'react';
import {
  Body, Container, Content, Icon, Left, ListItem, Right, Text,
} from 'native-base';
import { FlatList } from 'react-native';
import { useTranslation } from 'react-i18next';
import { DefaultScreenProps } from '../types/default';

const MoreSidebar = (props: DefaultScreenProps): React.FunctionComponentElement<DefaultScreenProps> => {
  const { navigation } = props;
  const { t } = useTranslation();

  const routes = [{
    route: 'Settings',
    text: t('settings.headerTitle'),
    icon: 'settings',
    iconColor: '#FF9501',
  }, {
    route: 'About',
    text: t('about.headerTitle'),
    icon: 'information-outline',
    iconColor: '#FF9501',
  }];

  return (
    <Container>
      <Content>
        <FlatList
          data={routes}
          keyExtractor={(item) => item.route}
          renderItem={({ item }): React.FunctionComponentElement<DefaultScreenProps> => (
            <ListItem
              icon
              key={item.route}
              onPress={(): void => {
                navigation.navigate(item.route);
                navigation.closeDrawer();
              }}
            >
              <Left>
                <Icon
                  active
                  name={item.icon}
                  type="MaterialCommunityIcons"
                />
              </Left>
              <Body>
                <Text>{item.text}</Text>
              </Body>
              <Right />
            </ListItem>
          )}
        />
      </Content>
    </Container>
  );
};

export default MoreSidebar;