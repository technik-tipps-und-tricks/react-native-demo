import React from 'react';
import {
  Body, Container, Content, Icon, Left, ListItem, Right, Text, Item,
} from 'native-base';
import { FlatList, SafeAreaView } from 'react-native';
import { useTranslation } from 'react-i18next';
import { DefaultProps, DefaultScreenProps } from '../types/default';

const Sidebar = (props: DefaultScreenProps): React.FunctionComponentElement<DefaultScreenProps> => {
  const { navigation } = props;
  const { t } = useTranslation();

  const routes = [{
    route: 'Home',
    text: t('home.headerTitle'),
    icon: 'home',
    iconColor: 'red',
  }, {
    route: 'Dummy',
    text: t('dummy.headerTitle'),
    icon: 'account',
    iconColor: '#FF9501',
  },
  {
    route: 'Settings',
    text: t('settings.headerTitle'),
    icon: 'settings',
    iconColor: '#FF9501',
  }];

  return (
    <Container>
      <Content>
        <FlatList
          data={routes}
          keyExtractor={(item) => item.route}
          renderItem={({ item }): React.FunctionComponentElement<DefaultProps> => (
            <ListItem
              icon
              key={item.route}
              onPress={(): void => {
                navigation.navigate(item.route);
                navigation.closeDrawer();
              }}
            >
              <Left>
                <Icon
                  active
                  name={item.icon}
                  type="MaterialCommunityIcons"
                />
              </Left>
              <Body>
                <Text>{item.text}</Text>
              </Body>
              <Right />
            </ListItem>
          )}
        />
      </Content>
    </Container>
  );
};

export default Sidebar;