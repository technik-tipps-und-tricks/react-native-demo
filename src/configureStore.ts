import { createStore, Store } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import { Persistor } from 'redux-persist/es/types';
import rootReducer from './reducers';

interface ConfigureStore {
  store: Store;
  persistor: Persistor;
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default (): ConfigureStore => {
  const store = createStore(persistedReducer);
  const persistor = persistStore(store);
  return { store, persistor };
};