import React from 'react';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { useSelector } from 'react-redux';
import * as TranslationDE from './translations/de.json';
import * as TranslationEN from './translations/en.json';
import { AppState } from './reducers/appReducer';
import { DefaultProps } from './types/default';


const I18nGate = (props: DefaultProps): React.FunctionComponentElement<DefaultProps> => {
  const { children } = props;
  const selectedLanguage = useSelector((state: { app: AppState}) => state.app.selectedLanguage);

  i18n.use(initReactI18next).init({
    resources: {
      de: {
        translation: {
          ...TranslationDE,
        },
      },
      en: {
        translation: {
          ...TranslationEN,
        },
      },
    },
    lng: selectedLanguage,
    fallbackLng: 'en',

    interpolation: {
      escapeValue: false,
    },
  });

  return (
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    { ...children }
  );
};

export default I18nGate;