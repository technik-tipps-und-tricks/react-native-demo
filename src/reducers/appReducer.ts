import {
  SET_SELECTED_LANGUAGE, SET_PASSWORD, AppActionTypes,
} from '../actions/appActions';

export interface AppState {
  selectedLanguage: string;
  password: string;
}

const initialState: AppState = {
  selectedLanguage: 'en',
  password: '',
};

const appReducer = (
  state = initialState,
  action: AppActionTypes,
): AppState => {
  switch (action.type) {
    case SET_SELECTED_LANGUAGE:
      return {
        ...state,
        selectedLanguage: action.payload.selectedLanguage,
      };
    case SET_PASSWORD:
      return {
        ...state,
        password: action.payload.password,
      };
    default:
      return state;
  }
};

export default appReducer;
