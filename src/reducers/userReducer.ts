import {
  SET_FIRST_NAME, SET_LAST_NAME, SET_PHONE_NUMBER, SET_EMAIL_ADDRESS,
  UserActionTypes,
} from '../actions/userActions';

export interface UserState {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  emailAddress: string;
}

const initialState: UserState = {
  firstName: '',
  lastName: '',
  phoneNumber: '',
  emailAddress: '',
};

const userReducer = (
  state = initialState,
  action: UserActionTypes,
): UserState => {
  switch (action.type) {
    case SET_FIRST_NAME:
      return {
        ...state,
        firstName: action.payload.firstName,
      };
    case SET_LAST_NAME:
      return {
        ...state,
        lastName: action.payload.lastName,
      };
    case SET_PHONE_NUMBER:
      return {
        ...state,
        phoneNumber: action.payload.phoneNumber,
      };
    case SET_EMAIL_ADDRESS:
      return {
        ...state,
        emailAddress: action.payload.emailAddress,
      };
    default:
      return state;
  }
};

export default userReducer;
