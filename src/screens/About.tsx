/**
 * The app info screen.
 *
 * @format
 */

import React from 'react';
import {
  Container, Content, Form, Text, View, Item, Thumbnail
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { useTranslation } from 'react-i18next';
import { StyleSheet, Linking } from 'react-native';
import Header from '../components/Header';
import { DefaultScreenProps } from '../types/default';

/**
 * get app name and version from JSON
 */
const appinfo = require('../../app.json');

const appicon = require('../../android/app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png');

const About = (props: DefaultScreenProps): React.FunctionComponentElement<DefaultScreenProps> => {
  //const { navigation } = props;
  const { t, i18n } = useTranslation();

  return (
      <Container>
        <Header navigation={props.navigation} title={t('about.headerTitle')} />
        <View> 
        <Item fixedLabel>
          <Grid style={styles.grid}>
            <Row style={styles.gridRow}>
              <Col style={styles.gridColumnThumbnail}>
                <Thumbnail square large style={styles.thumbnail} source={appicon}></Thumbnail>
              </Col>
              <Col style={styles.gridColumnVersion}>
                <Text style={styles.appname}>{appinfo.displayName}</Text>
                <Text style={styles.appversion}>Version {appinfo.versionName}</Text>
                <Text style={styles.hyperlink} onPress={() => Linking.openURL('http://www.technik-tipps-und-tricks.de')}>Technik Tipps und Tricks</Text>
              </Col>
            </Row>
          </Grid>
        </Item>
      </View>
        <Content>
        <View style={styles.descriptionView}>
          <Text>
            <Text style={styles.descriptionAppName}>{appinfo.displayName}</Text>
            <Text style={styles.descriptionText}> {t('about.description')}</Text>
          </Text>
        </View>
        <View style={styles.hyperlinkView}>
          <Text>{t('about.website')}: <Text style={styles.hyperlink} onPress={() => Linking.openURL('http://www.technik-tipps-und-tricks.de/programmierung/javascript/react-native/')}>React Native Tutorial</Text></Text>
        </View>
        </Content>
      </Container>
  );
};

const styles = StyleSheet.create({
  appname: {
    fontSize: 20,
    fontWeight: "bold" 
  },
  appversion: {
    fontSize: 16
  },
  grid: {
    width: 'auto', 
    height: 80, 
    margin: 10
  },
  gridRow: {
    height: 80
  },
  gridColumnThumbnail: {
    backgroundColor: 'white', 
    height: 80, 
    width: 80
  },
  thumbnail: {
    width: 80, 
    height: 80
  },
  gridColumnVersion: {
    backgroundColor: 'white', 
    height: 80, 
    paddingLeft: 10
  },
  descriptionView: {
    flex: 1, 
    flexDirection: 'row', 
    flexWrap: 'wrap', 
    padding: 20,
  },
  descriptionAppName: {
    fontStyle: 'italic', 
    fontWeight: 'bold', 
    flex: 1, 
    flexDirection: 'row', 
    flexWrap: 'wrap'
  },
  descriptionText: {
    flex: 2, 
      flexDirection: 'row', 
      flexWrap: 'wrap'
    },
  hyperlinkView: {
    alignItems: 'flex-start', 
    padding: 20, 
    paddingVertical: 5
  },
  hyperlink: {
    color: 'blue',
    textDecorationLine: 'underline',
    fontWeight: "bold"
  },
});

export default About;