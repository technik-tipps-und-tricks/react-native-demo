/**
 * The Home screen of the app with the default "Welcome to React" content.
 *
 * @format
 */

import React from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content, Form, Label } from 'native-base';
import { useTranslation } from 'react-i18next';
import Header from '../components/Header';
import {
  Header as ReactHeader,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { DefaultScreenProps } from '../types/default';

console.disableYellowBox = true;

const Home = (props: DefaultScreenProps): React.FunctionComponentElement<DefaultScreenProps> =>{
  //const { navigation } = props; 
  const { t, i18n } = useTranslation(); 

  return (
    <Container>
    <Header navigation={props.navigation} title={t('home.headerTitle')} />
      <Content>
        <ReactHeader />
        <Form style={styles.body}>
          <Label style={styles.sectionContainer}>
            <Label style={styles.sectionTitle}>Step One</Label>
            <Label style={styles.sectionDescription}>
              Edit <Label style={styles.highlight}>App.tsx</Label> to change
              this screen and then come back to see your edits.
            </Label>
          </Label>
          <Form style={styles.sectionContainer}>
            <Label style={styles.sectionTitle}>See Your Changes</Label>
            <Label style={styles.sectionDescription}>
              <ReloadInstructions />
            </Label>
          </Form>
          <Form style={styles.sectionContainer}>
            <Label style={styles.sectionTitle}>Debug</Label>
            <Label style={styles.sectionDescription}>
              <DebugInstructions />
            </Label>
          </Form>
          <Form style={styles.sectionContainer}>
            <Label style={styles.sectionTitle}>Learn More</Label>
            <Label style={styles.sectionDescription}>
              Read the docs to discover what to do next:
            </Label>
          </Form>
          <LearnMoreLinks />
        </Form>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
});

export default Home;
