/**
 * The app dummy screen for further use.
 *
 * @format
 */

import React from 'react';
import {
  Container, Content, Form, Text,
} from 'native-base';
import { useTranslation } from 'react-i18next';
import { StyleSheet, Linking } from 'react-native';
import Header from '../components/Header';
import { DefaultScreenProps } from '../types/default';

const NBVectorIcons = (props: DefaultScreenProps): React.FunctionComponentElement<DefaultScreenProps> => {
  //const { navigation } = props;
  const { t, i18n } = useTranslation();

  return (
      <Container>
        <Header navigation={props.navigation} title={t('vectoricons.headerTitle')} />
        <Content>
          <Form>
            <Text style={styles.text}>{t('vectoricons.description')}</Text>
            <Text>Link: 
              <Text style={styles.link} 
                onPress={() => Linking.openURL('https://github.com/oblador/react-native-vector-icons')}>
                {t('vectoricons.linkText')}
              </Text>
            </Text>
          </Form>
        </Content>
      </Container>
  );
};

const styles = StyleSheet.create({
  text: {
    color: 'red',
  },
  link: {
    color: "#1B95E0",
    textDecorationLine: "underline",
    fontWeight: "bold"
  },
});

export default NBVectorIcons;