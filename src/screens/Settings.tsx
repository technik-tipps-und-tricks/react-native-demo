/**
 * The app settings screen.
 *
 * @format
 */

import React from 'react';
import {
  Button, Container, Content, Form, Text, Item, Picker, Icon, Label, Input,
} from 'native-base';
import { useTranslation } from 'react-i18next';
import { StyleSheet, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Header from '../components/Header';
import { DefaultScreenProps } from '../types/default';
import { AppState } from '../reducers/appReducer';
import { setSelectedLanguage, SetPassword } from '../actions/appActions';

const Settings = (props: DefaultScreenProps): React.FunctionComponentElement<DefaultScreenProps> => {
  //const { navigation } = props;
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const selectedLanguage = useSelector((state: { app: AppState}) => state.app.selectedLanguage);
  const password = useSelector((state: { app: AppState}) => state.app.password);

  const onChangePassword = (newPassword: string): void => {
    dispatch(SetPassword(newPassword));
  };

  const savePassword = () => {
    Alert.alert(t('settings.savePassword'));
  };

  const onSelectedLanguageChange = (newSelectedLanguage: string): void => {
    i18n.changeLanguage(newSelectedLanguage).then((value) => {
      dispatch(setSelectedLanguage(newSelectedLanguage));
    });
  };

  return (
      <Container>
        <Header navigation={props.navigation} title={t('settings.headerTitle')} />
        <Content>
          <Form>
            <Item picker style={styles.settingsPickerItem}>
              <Label>
                {t('settings.language')}:{' '}
              </Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined }}
                iosHeader={t('settings.selectLanguage')}
                placeholder={t('settings.selectLanguage')}
                placeholderStyle={{ color: '#bfc6ea' }}
                placeholderIconColor="#007aff"
                selectedValue={selectedLanguage}
                onValueChange={onSelectedLanguageChange}
              >
                <Picker.Item label={t('settings.languages.german')} value="de" />
                <Picker.Item label={t('settings.languages.english')} value="en" />
              </Picker>
            </Item>
            <Item picker style={styles.settingsPickerItem}>
              <Label>
                {t('settings.password')}:{' '}
              </Label>
              <Input
                value={password}
                onChangeText={onChangePassword}
                secureTextEntry
                onEndEditing={savePassword}
              />
            </Item>
            <Button style={styles.saveButton}>
              <Text>{t('general.save')}</Text>
            </Button>
          </Form>
        </Content>
      </Container>
  );
};

const styles = StyleSheet.create({
  saveButton: {
    margin: 8,
    justifyContent: 'center',
  },
  settingsPickerItem: {
    marginLeft: 8,
  },
});

export default Settings;
