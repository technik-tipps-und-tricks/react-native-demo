import React from 'react';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';

export interface DefaultProps {
  children?: React.ReactNode;
}

export interface DefaultScreenProps extends DefaultProps{
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

export interface ExtendedScreenProps extends DefaultProps{
  menuNavigation: NavigationScreenProp<NavigationState, NavigationParams>;
  moreNavigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
